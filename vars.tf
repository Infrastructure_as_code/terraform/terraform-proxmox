variable "proxmox_host" {
  default = "192.168.***.***"
}

variable "ssh_key" {
  default = "ssh-ed25519 AAAAC**********************"
}

variable "dns_ip" {
  default = "1.1.1.1 8.8.8.8"
}

variable "virtual_machines" {
  default = {
    "ciusers" = "Users"
    "nfs-server" = {
      hostname   = "nfs-server"
      ip_address = "192.168.***.***/24"
      gateway    = "192.168.***.***",
      #vlan_tag = 100,
      target_node = "pve",
      cpu_cores   = 1,
      cpu_sockets = 1,
      memory      = "2048",
      storage     = "local-lvm"
      hdd_size    = "10",
      vm_template = "template-deb12",
    },
    "nfs-client" = {
      hostname   = "nfs-client"
      ip_address = "192.168.***.***/24"
      gateway    = "192.168.***.***",
      #vlan_tag = 100,
      target_node = "pve",
      cpu_cores   = 1,
      cpu_sockets = 1,
      memory      = "2048",
      storage     = "local-lvm"
      hdd_size    = "5",
      vm_template = "template-deb12",
    },
  }
}
