# Пример создания 2-х виртуальных машин в Proxmox из cloud-init шаблона.
Провайдер: [terraform-provider-proxmox](https://github.com/Telmate/terraform-provider-proxmox)

## VARS
* **proxmox_host**      - IP адрес Proxmox
* **ssh_key**           - Public SSH key для удаленного подключения
* **dns_ip**            - IP адрес DNS сервера 
* **virtual_machines**
    * **ciusers**       - Имя пользователя
    * **hostname**      - Имя ВМ
    * **ip_address**    - IP ВМ
    * **gateway**       - IP Gateway
    * **vlan_tag**      - Vlan tag
    * **target_node**   - Имя сервера Proxmox 
    ![Имя сервера Proxmox](./img/target_node.png)
    * **cpu_cores**     - Количество ядер
    * **cpu_sockets**   - Количество сокетов
    * **memory**        - Количество ОЗУ
    * **storage**       - Название хранилища
    ![Название хранилища](./img/storage.png)
    * **hdd_size**      - Объем диска
    * **vm_template**   - Имя cloud-init шаблона

[Туториал по созданию cloud-init шаблона.](https://github.com/mmmex/proxmox-cloud-init-tutorial)