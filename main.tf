provider "proxmox" {
  pm_api_url      = "https://${var.proxmox_host}:8006/api2/json"
  pm_tls_insecure = true

  # Раскомментировать для дебага.
  # pm_log_enable = true
  # pm_log_file = "terraform-plugin-proxmox.log"
  # pm_debug = true
  # pm_log_levels = {
  # _default = "debug"
  # _capturelog = ""
  # }
}

resource "proxmox_vm_qemu" "virtual_machines" {
  for_each = var.virtual_machines

  name        = each.value.hostname
  target_node = each.value.target_node
  clone       = each.value.vm_template
  # Activate QEMU agent for this VM
  agent = "1"
  # HA
  # hastate     = "started"
  # hagroup     = "HA" # Имя HA группы
  # Start on boot
  onboot = true

  cloudinit_cdrom_storage = each.value.storage
  ciuser                  = each.value.ciusers

  os_type  = "cloud-init"
  cores    = each.value.cpu_cores
  sockets  = each.value.cpu_sockets
  cpu      = "host"
  memory   = each.value.memory
  scsihw   = "virtio-scsi-pci"
  bootdisk = "scsi0"
  disks {
    scsi {
      scsi0 {
        disk {
          size    = each.value.hdd_size
          storage = each.value.storage
        }
      }
    }
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
    #tag = each.value.vlan_tag
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  # Cloud-init config
  ipconfig0  = "ip=${each.value.ip_address},gw=${each.value.gateway}"
  nameserver = var.dns_ip
  sshkeys    = var.ssh_key
}

output "vm_ipv4_addresses" {
  value = {
    for instance in proxmox_vm_qemu.virtual_machines :
    instance.name => instance.default_ipv4_address
  }
}
